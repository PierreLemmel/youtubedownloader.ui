using NFluent;
using NUnit.Framework;

namespace YoutubeDownloader.Helpers
{
    public class YoutubeHelpersShould
    {
        [Test]
        [TestCase("https://music.youtube.com/watch?v=HJRL-85fRK0", "HJRL-85fRK0")]
        [TestCase("https://music.youtube.com/watch?v=s-WZO_iy-As&list=RDAMVMHJRL-85fRK0", "s-WZO_iy-As")]
        [TestCase("https://music.youtube.com/watch?list=RDAMVMHJRL-85fRK0&v=s-WZO_iy-As", "s-WZO_iy-As")]
        [TestCase("https://www.youtube.com/watch?v=b9fUdJdlExU", "b9fUdJdlExU")]
        public void FindCorrectParameterWhenExists(string input, string expected)
        {
            bool success = YoutubeHelpers.TryExtractVideoParameter(input, out string? param);

            Check.That(success).IsTrue();
            Check.That(param).IsEqualTo(expected);
        }

        [Test]
        [TestCase("https://music.youtube.com/channel/UCQd0fAjAhMcd5qLBJXI0-eQ")]
        [TestCase("https://music.youtube.com/search?q=stupeflip")]
        [TestCase("https://mail.google.com/mail/u/0/#inbox")]
        [TestCase("www.google.com")]
        public void ReturnFalseWhenInvalidUrl(string input)
        {
            bool success = YoutubeHelpers.TryExtractVideoParameter(input, out _);

            Check.That(success).IsFalse();
        }
    }
}