using System;

namespace YoutubeDownloader.Models
{
    public record SettingModel(string Key, string Value);

    public record VideoModel(string Id, string Title);
}