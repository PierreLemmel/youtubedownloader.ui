using System;
using System.Collections.Generic;
using System.Linq;

namespace YoutubeDownloader
{
    public static class Booleans
    {
        public static bool Or(params bool[] input) => input.Any(b => b) || input.IsEmpty();
    }
}