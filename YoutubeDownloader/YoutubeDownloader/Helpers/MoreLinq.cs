using System;
using System.Collections.Generic;
using System.Linq;

namespace YoutubeDownloader
{
    public static class MoreLinq
    {
        public static bool IsEmpty<T>(this IEnumerable<T> sequence) => !sequence.Any();
    }
}