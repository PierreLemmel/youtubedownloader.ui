using System.Linq;

using System.Diagnostics.CodeAnalysis;

namespace YoutubeDownloader.Helpers
{
    public static class YoutubeHelpers
    {
        public static bool TryExtractVideoParameter(string input, [NotNullWhen(true)] out string? param)
        {
            param = null;
            string[] urlChunks = input.Split("?");

            if (urlChunks.Length != 2) return false;

            string search = urlChunks[1];

            param = search.Split("&").Select(p =>
                {
                    string[] pChunks = p.Split("=");
                    return (key: pChunks[0], value: pChunks[1]);
                })
                .Where(kvp => kvp.key == "v")
                .Select(kvp => kvp.value)
                .FirstOrDefault();

            return param is not null;
        }
    }
}