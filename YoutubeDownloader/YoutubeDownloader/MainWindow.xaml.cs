﻿using Ookii.Dialogs.Wpf;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using VideoLibrary;
using Xabe.FFmpeg;
using YoutubeDownloader.Helpers;
using YoutubeDownloader.Models;
using YoutubeDownloader.Services;
using YoutubeDownloader.ViewModels;

namespace YoutubeDownloader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly YTDViewModel viewModel;
        private readonly ControlViewModel control;
        private readonly SettingsViewModel settings;
        private readonly MaintenanceViewModel maintenance;

        private readonly DispatcherTimer timer;

        private readonly IDataAccess dataAccess;

        private readonly string tempDir = "Temp";

        public MainWindow()
        {
            control = new();
            Log.MessageSent += OnLogMessage;

            Log.Trace("Setting up FFMPEG");
            FFmpeg.SetExecutablesPath("FFMPEG");
            Log.Trace("FFMPEG set up");

            dataAccess = new DataAccess();

            dataAccess.CreateDatabaseIfNeeded();
            dataAccess.CreateSchemaIfNeeded();


            string libraryFolder = dataAccess.GetSetting(Settings.LibraryFolderKey)?.Value ?? Settings.DefaultLibraryFolder;
            string sessionsFolder = dataAccess.GetSetting(Settings.SessionsFolderKey)?.Value ?? Settings.DefaultSessionsFolder;
            string currentSession = dataAccess.GetSetting(Settings.CurrentSessionKey)?.Value ?? Settings.DefaultSession;
            settings = new(libraryFolder, sessionsFolder, currentSession);

            maintenance = new();

            viewModel = new(control, settings, maintenance);

            DataContext = viewModel;

            if (!Directory.Exists(tempDir))
            {
                Log.Trace($"Create Temp directory: '{tempDir}'");
                Directory.CreateDirectory(tempDir);
            }

            timer = new();
            timer.Interval = TimeSpan.FromMilliseconds(100);
            timer.Start();
            timer.Tick += Timer_Tick;

            InitializeComponent();
        }

        private string lastClipboard = "";
        private void Timer_Tick(object? sender, EventArgs e)
        {
            string clipboard = Clipboard.GetText();
            if (clipboard != lastClipboard)
            {
                OnClipboardContentChanged(clipboard);
                lastClipboard = clipboard;
            }
        }

        private void OnClipboardContentChanged(string clipboard)
        {
            if (clipboard.StartsWith("ytd: "))
            {
                string url = clipboard.Substring(5);

                Log.Info($"Received YTD command from browser: '{url}'");
                if (YoutubeHelpers.TryExtractVideoParameter(url, out string? v))
                {
                    if (dataAccess.TryGetVideo(v, out VideoModel? video))
                    {
                        Log.Trace($"Found video '{v}' in cache");
                        //...
                    }
                    else
                    {
                        Task.Run(() => ImportVideoAsync(url, v));
                    }
                }
                else
                {
                    Log.Warning($"Invalid YTD url: '{url}'");
                }
            }
        }

        private async Task ImportVideoAsync(string url, string v)
        {
            Stopwatch watch = Stopwatch.StartNew();
            Log.Info($"Importing video: '{v}'");

            Log.Trace($"Downloading video: '{v}'");
            YouTube youtube = YouTube.Default;
            YouTubeVideo video = await youtube.GetVideoAsync(url);
            Log.Trace($"Video downloaded: '{v}'");

            string tempFileMp3 = Path.Combine(tempDir, $"Temp_{v}.mp3");
            string tempFileMp4 = Path.Combine(tempDir, $"Temp_{v}.mp4");

            if (File.Exists(tempFileMp3)) File.Delete(tempFileMp3);
            if (File.Exists(tempFileMp4)) File.Delete(tempFileMp4);

            Log.Trace($"Creating temp file: '{v}'");
            using (Stream vs = video.Stream())
            using (FileStream fs = File.Create(tempFileMp4))
            {
                await vs.CopyToAsync(fs);
            }
            Log.Trace($"Temp file created: '{v}'");

            Log.Trace($"Obtaining MediaInfo: '{v}'");
            IMediaInfo mediaInfo = await FFmpeg.GetMediaInfo(tempFileMp4);
            Log.Trace($"MediaInfo obtained: '{v}'");

            Log.Trace($"Converting video: '{v}'");
            IConversionResult conversionResult = await FFmpeg.Conversions.New()
                .AddStream(mediaInfo.AudioStreams)
                .SetOutput(tempFileMp3)
                .Start();
            Log.Trace($"Video converted: '{v}'");

            string mp3FinalPath = Path.Combine(settings.LibraryFolder, $"{video.Title}.mp3");

            File.Move(tempFileMp3, mp3FinalPath);
            File.Delete(tempFileMp4);

            Log.Info($"Video imported: '{v}' in {watch.ElapsedMilliseconds}ms");
            Log.Info($"Full path: '{mp3FinalPath}'");

            VideoModel videoModel = new(v, video.Title);
            dataAccess.AddVideo(videoModel);
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            timer.Stop();
            timer.Tick -= Timer_Tick;

            SaveLibraryFolderSetting();
            SaveSessionsFolderSetting();
            SaveCurrentSessionSetting();

            base.OnClosing(e);
        }

        private void SaveLibraryFolderSetting() => SaveSetting(Settings.LibraryFolderKey, settings.LibraryFolder);
        private void SaveSessionsFolderSetting() => SaveSetting(Settings.SessionsFolderKey, settings.SessionsFolder);
        private void SaveCurrentSessionSetting() => SaveSetting(Settings.CurrentSessionKey, settings.CurrentSession);

        private void SaveSetting(string key, string value)
        {
            SettingModel setting = new(key, value);
            dataAccess.AddOrUpdateSetting(setting);
        }

        private void OnLogMessage(object? sender, LogEventArgs e) => Dispatcher.Invoke(() =>
        {
            const string TimestampFormat = "HH:mm:ss.fff";
            control.LogMessages.Add(new(e.TimeStamp.ToString(TimestampFormat), e.Level.ToString(), e.Message));
        });

        private void OnModifyLibraryFolderClicked(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new()
            {
                ShowNewFolderButton = true,
                SelectedPath = settings.LibraryFolder
            };
            bool result = dialog.ShowDialog() ?? false;
            if (result)
            {
                settings.LibraryFolder = dialog.SelectedPath;
                SaveLibraryFolderSetting();
            }
        }

        private void OnModifySessionsFolderClicked(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new()
            {
                ShowNewFolderButton = true,
                SelectedPath = settings.SessionsFolder
            };
            bool result = dialog.ShowDialog() ?? false;
            if (result)
            {
                settings.SessionsFolder = dialog.SelectedPath;
                SaveSessionsFolderSetting();
            }
        }

        private void OnChangeSessionClicked(object sender, RoutedEventArgs e)
        {
            settings.CurrentSession = currentSessionText.Text;
            SaveCurrentSessionSetting();

            MessageBox.Show($"Current session is now '{settings.CurrentSession}'");
        }
    }
}