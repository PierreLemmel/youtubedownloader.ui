﻿using YoutubeDownloader.Models;

namespace YoutubeDownloader.Services
{
    public interface IDataAccess
    {
        SettingModel AddOrUpdateSetting(SettingModel setting);
        VideoModel AddVideo(VideoModel video);
        bool CreateDatabaseIfNeeded();
        bool CreateSchemaIfNeeded();
        SettingModel? GetSetting(string key);
        VideoModel? GetVideo(string id);
    }
}