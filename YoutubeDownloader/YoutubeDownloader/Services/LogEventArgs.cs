﻿using System;

namespace YoutubeDownloader.Services
{
    public class LogEventArgs
    {
        public DateTime TimeStamp { get; }
        public Log.LogLevel Level { get; }
        public string Message { get; }

        public LogEventArgs(Log.LogLevel level, string message)
        {
            TimeStamp = DateTime.Now;
            Level = level;
            Message = message;
        }
    }
}