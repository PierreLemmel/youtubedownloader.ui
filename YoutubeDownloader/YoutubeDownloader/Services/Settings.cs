using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace YoutubeDownloader.Services
{
    public static class Settings
    {
        public static string LibraryFolderKey => "LibraryFolder";
        public static string DefaultLibraryFolder => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "YTD", "Library");

        public static string SessionsFolderKey => "SessionsFolder";
        public static string DefaultSessionsFolder => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "YTD", "Sessions");

        public static string CurrentSessionKey => "CurrentSession";
        public static string DefaultSession => "DefaultSession";
    }
}