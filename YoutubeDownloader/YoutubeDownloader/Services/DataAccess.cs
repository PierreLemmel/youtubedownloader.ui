using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using YoutubeDownloader.Models;

namespace YoutubeDownloader.Services
{
    public class DataAccess : IDataAccess
    {
        private const string dbPath = "ytddata.db";

        private readonly string connectionString;

        public DataAccess()
        {
            SQLiteConnectionStringBuilder csb = new()
            {
                Uri = $"file:{dbPath}"
            };

            connectionString = csb.ToString();
        }

        public bool CreateDatabaseIfNeeded()
        {
            bool createDb = !File.Exists(dbPath);

            if (createDb)
            {
                Log.Info($"Creating database '{dbPath}'");
                SQLiteConnection.CreateFile(dbPath);
                Log.Info($"Database '{dbPath}' created");
            }
            else
                Log.Trace($"Database found at path '{dbPath}'");

            return createDb;
        }

        private bool CreateTableIfNeeded(string tableName, string creationQuery)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            string checkQuery = Queries.CheckTableExistence(tableName);
            bool tableExists = connection.ExecuteScalar<bool>(checkQuery);

            if (tableExists)
            {
                Log.Trace($"Table '{tableName}' already exists");
                return false;
            }
            else
            {
                Log.Trace($"Creating table '{tableName}'");
                connection.Execute(creationQuery);
                Log.Trace($"Table '{tableName}' created");

                return true;
            }
        }

        public bool CreateSchemaIfNeeded() => Booleans.Or(
            CreateTableIfNeeded("Settings", Queries.CreateSettingsTable),
            CreateTableIfNeeded("Videos", Queries.CreateVideosTable)
        );

        private static class Queries
        {
            public static string CreateSettingsTable => "CREATE TABLE Settings ([Key] TEXT(8, 64) PRIMARY KEY NOT NULL, Value NOT NULL)";
            public static string CreateVideosTable => "CREATE TABLE Videos (Id TEXT(11, 11) PRIMARY KEY NOT NULL, Title TEXT(4, 80) NOT NULL)";

            public static string AddSetting => "INSERT INTO Settings ([Key], Value) VALUES (@Key, @Value)";
            public static string GetSetting => "SELECT * FROM Settings WHERE [Key] = @Key";
            public static string UpdateSetting => "UPDATE Settings Set Value = @Value WHERE [Key] = @Key";

            public static string AddVideo => "INSERT INTO Videos (Id, Title) VALUES (@Id, @Title)";
            public static string GetVideo => "SELECT * FROM Videos WHERE Id = @Id";

            public static string CheckTableExistence(string table) => $"SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='{table}'";
        }

        public SettingModel? GetSetting(string key)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            SettingModel? result = connection.QuerySingleOrDefault<SettingModel>(Queries.GetSetting, new { Key = key });
            return result;
        }

        public SettingModel AddOrUpdateSetting(SettingModel setting)
        {
            bool exists = GetSetting(setting.Key) is not null;

            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            if (exists)
            {
                connection.Execute(Queries.UpdateSetting, setting);
            }
            else
            {
                connection.Execute(Queries.AddSetting, setting);
            }

            return setting;
        }

        public VideoModel? GetVideo(string id)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            VideoModel? result = connection.QuerySingleOrDefault<VideoModel>(Queries.GetVideo, new { Id = id });
            return result;
        }

        public VideoModel AddVideo(VideoModel video)
        {
            using SQLiteConnection connection = CreateConnection();
            connection.Open();

            connection.Execute(Queries.AddVideo, video);
            return video;
        }

        private SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqliteConnection = new SQLiteConnection(connectionString);
            sqliteConnection.StateChange += OnStateChanged;

            return sqliteConnection;
        }

        private void OnStateChanged(object sender, StateChangeEventArgs e)
        {
            SQLiteConnection sqliteConnection = (SQLiteConnection)sender;

            if (e.CurrentState == ConnectionState.Open)
                sqliteConnection.Trace += OnTraceEvent;
            else if (e.CurrentState == ConnectionState.Closed)
                sqliteConnection.Trace -= OnTraceEvent;
        }

        private void OnTraceEvent(object sender, TraceEventArgs e) => Log.Trace($"Executing Query: \"{e.Statement}\"");
    }
}