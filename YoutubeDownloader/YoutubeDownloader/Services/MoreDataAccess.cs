using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using YoutubeDownloader.Models;

namespace YoutubeDownloader.Services
{
    public static class MoreDataAccess
    {
        public static bool TryGetVideo(this IDataAccess dataAccess, string id, [NotNullWhen(true)] out VideoModel? video)
        {
            video = dataAccess.GetVideo(id);
            return video is not null;
        }
    }
}