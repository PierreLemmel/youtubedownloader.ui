using System;
using System.Collections.Generic;
using System.Linq;

namespace YoutubeDownloader.Services
{
    public static class Log
    {
        public static LogLevel Level { get; private set; }
        public static void SetLogLevel(LogLevel level) => Level = level;

        static Log()
        {
#if DEBUG
            Level = LogLevel.Debug;
#elif RELEASE
            Level = LogLevel.Info;
#else
#error Unexpected configuration
#endif
        }

        public static void Debug(object obj) => Debug(obj?.ToString() ?? "");
        public static void Debug(string message) => LogMessage(LogLevel.Debug, message);
        public static void Trace(object obj) => Trace(obj?.ToString() ?? "");
        public static void Trace(string message) => LogMessage(LogLevel.Trace, message);
        public static void Info(object obj) => Info(obj?.ToString() ?? "");
        public static void Info(string message) => LogMessage(LogLevel.Info, message);
        public static void Warning(object obj) => Warning(obj?.ToString() ?? "");
        public static void Warning(string message) => LogMessage(LogLevel.Warning, message);
        public static void Error(object obj) => Error(obj?.ToString() ?? "");
        public static void Error(string message) => LogMessage(LogLevel.Error, message);

        private static void LogMessage(LogLevel level, string message)
        {
            if (level >= Level)
                MessageSent?.Invoke(null, new(level, message));
        }

        public static event EventHandler<LogEventArgs>? MessageSent;

        public enum LogLevel
        {
            Debug = 0,
            Trace = 1,
            Info = 2,
            Warning = 3,
            Error = 4
        }
    }
}