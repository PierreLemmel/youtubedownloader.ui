using System;
using System.Collections.Generic;
using System.Linq;

namespace YoutubeDownloader.ViewModels
{
    public class SettingsViewModel : ViewModel
    {
        public SettingsViewModel(string libraryFolder, string sessionsFolder, string currentSession)
        {
            this.libraryFolder = libraryFolder;
            this.sessionsFolder = sessionsFolder;
            this.currentSession = currentSession;
        }

        private string libraryFolder;
        public string LibraryFolder
        {
            get => libraryFolder;
            set
            {
                libraryFolder = value;
                OnPropertyChanged(nameof(LibraryFolder));
            }
        }

        private string sessionsFolder;
        public string SessionsFolder
        {
            get => sessionsFolder;
            set
            {
                sessionsFolder = value;
                OnPropertyChanged(nameof(SessionsFolder));
            }
        }

        private string currentSession;
        public string CurrentSession
        {
            get => currentSession;
            set
            {
                currentSession = value;
                OnPropertyChanged(nameof(CurrentSession));
            }
        }
    }
}