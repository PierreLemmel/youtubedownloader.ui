using System;
using System.ComponentModel;

namespace YoutubeDownloader.ViewModels
{
    public class YTDViewModel : ViewModel
    {
        public ControlViewModel Control { get; }
        public SettingsViewModel Settings { get; }
        public MaintenanceViewModel Maintenance { get; }

        public YTDViewModel(ControlViewModel control, SettingsViewModel settings, MaintenanceViewModel maintenance)
        {
            Control = control;
            Settings = settings;
            Maintenance = maintenance;
        }
    }
}