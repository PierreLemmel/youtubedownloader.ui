using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace YoutubeDownloader.ViewModels
{
    public class ControlViewModel : ViewModel
    {
        public ObservableCollection<LogMessageViewModel> LogMessages { get; } = new();
    }
}