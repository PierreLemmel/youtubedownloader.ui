using System;
using System.Collections.Generic;
using System.Linq;

namespace YoutubeDownloader.ViewModels
{
    public class LogMessageViewModel : ViewModel
    {
        public LogMessageViewModel(string timeStamp, string level, string message)
        {
            TimeStamp = timeStamp;
            Level = level;
            Message = message;
        }

        public string TimeStamp { get; }
        public string Level { get; }
        public string Message { get; }
    }
}